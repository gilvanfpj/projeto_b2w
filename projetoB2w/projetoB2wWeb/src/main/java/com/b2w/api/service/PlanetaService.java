package com.b2w.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2w.api.model.Planeta;
import com.b2w.api.repository.PlanetaRepository;

@Service
public class PlanetaService {

	@Autowired
    private PlanetaRepository planetaRepository;
 
    public Planeta cadastrar(Planeta planeta) {
    	return planetaRepository.insert(planeta);
    }
 
    public List<Planeta> listar() {
       return planetaRepository.findAll();
    }
 
    public Planeta buscarPorNome(String nome) {
       return planetaRepository.buscarPorNome(nome);
    }
    
    public Planeta buscarPorId(String id) {
    	return planetaRepository.buscarPorId(id);
     }
 
    public void excluir(Planeta planeta) {
    	planetaRepository.delete(planeta);
    }
	
}
