package com.b2w.api.model;

import java.util.List;
import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "planeta")
public class Planeta {

	@Id
	private String id;
	
	@Field("name")
	@NotBlank
	private String nome;
	
	@Field("terrain")
	@NotBlank
	private String terreno;
	
	@Field("climate")
	@NotBlank
	private String clima;
	
	private List<String> films;
	
	@Transient
	private Integer qtdFilmes;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTerreno() {
		return terreno;
	}
	public void setTerreno(String terreno) {
		this.terreno = terreno;
	}
	public String getClima() {
		return clima;
	}
	public void setClima(String clima) {
		this.clima = clima;
	}
	public Integer getQtdFilmes() {
		if(films != null) {
			return films.size();
		}else {
			return 0;
		}
	}
	public void setQtdFilmes(Integer qtdFilmes) {
		this.qtdFilmes = qtdFilmes;
	}
	public List<String> getFilms() {
		return films;
	}
	public void setFilms(List<String> films) {
		this.films = films;
	}
	
}
