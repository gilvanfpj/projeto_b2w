package com.b2w.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoB2wWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoB2wWebApplication.class, args);
	}
}
