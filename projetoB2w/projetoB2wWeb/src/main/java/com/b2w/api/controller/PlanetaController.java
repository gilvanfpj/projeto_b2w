package com.b2w.api.controller;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.b2w.api.model.Planeta;
import com.b2w.api.service.PlanetaService;

@RestController
@RequestMapping("/planetaRest")
public class PlanetaController {
		
	@Autowired
	PlanetaService planetaService;
	
	@GetMapping(produces="application/json")
	public @ResponseBody List<Planeta> listar() {		
		return planetaService.listar();
	}
	
	@GetMapping(produces="application/json")
	@RequestMapping(method = RequestMethod.GET, value = "buscarPorNome/{nome}")
	public @ResponseBody Planeta buscarPorNome(@PathVariable("nome") String nome) {
		return planetaService.buscarPorNome(nome);
	}
	
	@GetMapping(produces="application/json")
	@RequestMapping(method = RequestMethod.GET, value = "buscarPorId/{id}")
	public @ResponseBody Planeta buscarPorID(@PathVariable("id") String id) {
		return planetaService.buscarPorId(id);
	}
	
	@PostMapping()
	public ResponseEntity<Planeta> cadastrar(@RequestBody @Valid Planeta planeta){
		planetaService.cadastrar(planeta);
		return new ResponseEntity<Planeta>(planeta, HttpStatus.OK);
	}
	
	@DeleteMapping()
	public ResponseEntity<Planeta> excluir(@RequestBody Planeta planeta){
		planetaService.excluir(planeta);
		return new ResponseEntity<Planeta>(planeta, HttpStatus.OK);
	}
		
}
