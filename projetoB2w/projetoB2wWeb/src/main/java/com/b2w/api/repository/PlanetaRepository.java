package com.b2w.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.b2w.api.model.Planeta;

public interface PlanetaRepository extends MongoRepository<Planeta, String> {
	
	@Query("{'nome' : ?0}")
    Planeta buscarPorNome(String nome);
 
    @Query(value = "{'id' : ?0}")
    Planeta buscarPorId(String id);
}
 
